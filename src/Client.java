
import org.example.Employee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class Client {
    private static final String URL = "jdbc:mysql://localhost:3306/new_schema-client";
    private static final String USERNAME = "root";
    private static final int PASSWORD = 1234;


    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Scanner scanner = new Scanner(System.in);
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection(URL, USERNAME, String.valueOf(PASSWORD));
        Statement statement = connection.createStatement();
        statement.execute("insert into users (name, connection_time, client_socket_data)" +
                " values ('Clara', '28.07.2023', 'some_info')");

        try {
            // Підключення до сервера
            Socket socket = new Socket("localhost", 1234);

            // Отримання потоку введення та виведення
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);

            // Відправлення повідомлення на сервер
            String message = new String();
            while (!message.equals("close")) {
                message = scanner.nextLine();
                writer.println(message);
                System.out.println("Повідомлення надіслано на сервер: " + message);
                if (message.equals("close")) {
                    statement.execute("delete from users where name = 'Clara'");
                }
                // Отримання відповіді від сервера
                String response = reader.readLine();
                System.out.println("Відповідь від сервера: " + response);
            }
            reader.close();
            writer.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}