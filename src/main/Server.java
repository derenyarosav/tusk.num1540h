package main;

import java.io.*;
import java.net.*;

public class Server {
    public static void main(String[] args) {
        try {
            // Створення серверного сокету та очікування з'єднання
            ServerSocket serverSocket = new ServerSocket(1234);
            System.out.println("Сервер запущено. Очікування з'єднання...");

            Socket socket = serverSocket.accept();
            System.out.println("З'єднання встановлено з клієнтом: " + socket.getInetAddress());

            // Отримання потоку введення та виведення
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);



            String message = new String();
            while (!message.equals("close")) {
                // Отримання повідомлення від клієнта
                message = reader.readLine();
                System.out.println("Повідомлення від клієнта: " + message);
                // Відправлення відповіді на клієнта
                String response = "Привіт, клієнт!";
                writer.println(response);
                System.out.println("Відповідь надіслана на клієнта: " + response);
            }
            // Закриття ресурсів
            reader.close();
            writer.close();
            socket.close();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}