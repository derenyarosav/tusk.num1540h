package org.example;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
public class Employee {
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "connection_time")
    private LocalDateTime connection_time;
    @Column(name = "client_socket_data")
    private String client_socket_data;


    public Employee(String name, LocalDateTime connection_time, String client_socket_data) {
        this.name = name;
        this.connection_time = connection_time;
        this.client_socket_data = client_socket_data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getConnection_time() {
        return connection_time;
    }

    public void setConnection_time(LocalDateTime connection_time) {
        this.connection_time = connection_time;
    }

    public String getClient_socket_data() {
        return client_socket_data;
    }

    public void setClient_socket_data(String client_socket_data) {
        this.client_socket_data = client_socket_data;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", connection_time=" + connection_time +
                ", client_socket_data='" + client_socket_data + '\'' +
                '}';
    }
}
